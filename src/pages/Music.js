import React from "react";
import ReactHowler from 'react-howler';
import Reproductor from '../Components/Reproductor';

class Music extends React.Component{
    
    render () {
        return (
        <div>

            {this.props.list.map(song => 
                <div>
                    <a href="http://">
                        <h3>{song.artist} - {song.title}</h3>                    
                    </a>                    
                </div>
                )
            }

            <Reproductor/>
            
        </div>
        );
    }
}
export default Music;