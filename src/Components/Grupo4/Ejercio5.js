import React from 'react'
import styled from 'styled-components'
import inicio from './Inicio'
import Blog from './Blog'
import Tienda from './Tienda'
import {NavLink , Switch, Route} from 'react-router-dom'

const Ejercio5  = () => {
    return (

        <Contenedor>
         <Menu>
         <NavLink to="/Inicio">Inicio</NavLink>
         <NavLink to="/Blog">Blog</NavLink>
         <NavLink to="/tienda">Tienda</NavLink>

         </Menu>


        <main>

        <Switch>
            <Route  exact path="/inicio"  component={inicio} ></Route>
            <Route path="/Blog" component={Blog} ></Route>
            <Route path="/Tienda" component={Tienda}></Route>

          </Switch>
        </main>
         

         
        </Contenedor>


      );
}

const Contenedor = styled.div`
    max-width: 1000px;
    padding: 40px;
    width: 90%;
    display: grid;
    gap: 20px;
    grid-template-columns: 2fr 1fr;
    background: #fff;
    margin: 40px 0;
    border-radius: 10px;
    box-shadow: 0px 0px 5px rgba(129, 129, 129, 0.1);
`;
 
const Menu = styled.nav`
    width: 100%;
    text-align: center;
    background: #092c4c;
    grid-column: span 2;
    border-radius: 3px;
 
    a {
        color: #fff;
        display: inline-block;
        padding: 15px 20px;
    }
 
    a:hover {
        background: #1d85e8;
        text-decoration: none;
    }
`;
 
export default Ejercio5 ;